Namespace.declare("net.lugdunon.currency.gsc");
Namespace.newClass("net.lugdunon.currency.gsc.Currency","net.lugdunon.state.currency.ICurrency");

net.lugdunon.currency.gsc.Currency.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.currency.gsc.Currency,"init",[initData]);
	
	if(initData == null)
	{
		this.g=0;
		this.s=0;
		this.c=0;
	}
	else if(initData.g != null)
	{
		this.n=initData.n==true;
		this.g=initData.g;
		this.s=initData.s;
		this.c=initData.c;
	}
	else if(typeof initData == "number")
	{
		this.fromPrimitive(initData              );
	}
	else
	{
		this.fromPrimitive(initData.readFloat64());
	}
	
	return(this);
};

net.lugdunon.currency.gsc.Currency.prototype.fromPrimitive=function(value)
{
	if(value < 0)
	{
		this.n=true;
		value =Math.abs(value);
	}
	
	this.g=Math.floor( value/         10000               );
	this.s=Math.floor((value- (this.g*10000))/100         );
	this.c=Math.floor( value-((this.g*10000)+(this.s*100)));
};

net.lugdunon.currency.gsc.Currency.prototype.toPrimitive=function()
{
	return((this.n?-1:1)*((this.g*10000)+(this.s*100)+this.c));
};

net.lugdunon.currency.gsc.Currency.prototype.add=function(currency)
{
	this.fromPrimitive(this.toPrimitive()+currency.toPrimitive());
};

net.lugdunon.currency.gsc.Currency.prototype.subtract=function(currency)
{
	this.fromPrimitive(this.toPrimitive()-currency.toPrimitive());
};

net.lugdunon.currency.gsc.Currency.prototype.multiply=function(multiplier)
{
	this.fromPrimitive(Math.round(this.toPrimitive()*multiplier));
};

net.lugdunon.currency.gsc.Currency.prototype.toString=function()
{
	return((this.n?"- ":"")+this.g+"g "+this.s+"s "+this.c+"c");
};

net.lugdunon.currency.gsc.Currency.prototype.toShortString=function()
{
	var gg=((this.g==0)?(null):(this.g+"g"));
	var ss=((this.s==0)?(null):(this.s+"s"));
	var cc=((this.c==0)?(null):(this.c+"c"));
	var v;
	
	if(gg != null)
	{
		v=gg;
		
		if(ss != null)
		{
			v+=" "+ss;
		}
		
		if(cc != null)
		{
			v+=" "+cc;
		}
	}
	else if(ss != null)
	{
		v=ss;
		
		if(cc != null)
		{
			v+=" "+cc;
		}
	}
	else if(cc != null)
	{
		v=cc;
	}
	
	return((this.n?"-":"")+v);
};

net.lugdunon.currency.gsc.Currency.prototype.toDisplayString=function()
{
	return(
		(this.n?"-&nbsp;":"")+
		"<span style='color:#d1b65a;'>"+
		this.g+
		"g</span> <span style='color:#adadad;'>"+
		this.s+
		"s</span> <span style='color:#d66e26;'>"+
		this.c+
		"c</span>"
	);
};

net.lugdunon.currency.gsc.Currency.prototype.toShortDisplayString=function()
{
	if(this.g==0 && this.s == 0 && this.c == 0)
	{
		return((this.n?"-&nbsp;":"")+"<span style='color:#d66e26;'>0c</span>");
	}
	
	var gg=((this.g==0)?(null):("<span style='color:#d1b65a;'>"+this.g+"g</span>"));
	var ss=((this.s==0)?(null):("<span style='color:#adadad;'>"+this.s+"s</span>"));
	var cc=((this.c==0)?(null):("<span style='color:#d66e26;'>"+this.c+"c</span>"));
	var v;
	
	if(gg != null)
	{
		v=gg;
		
		if(ss != null)
		{
			v+=" "+ss;
		}
		
		if(cc != null)
		{
			v+=" "+cc;
		}
	}
	else if(ss != null)
	{
		v=ss;
		
		if(cc != null)
		{
			v+=" "+cc;
		}
	}
	else if(cc != null)
	{
		v=cc;
	}
	
	return((this.n?"-":"")+v);
};

/////

net.lugdunon.currency.gsc.Currency.prototype.renderEditUI=function(parent)
{
	parent.append("<input required='required' type='integer' style='text-align:right;width:64px;' name='g'/><span class='strokedAndShadowed' style='color:#d1b65a;font-size:18px;margin-right:16px;margin-left:8px;'>g</span>");
	parent.append("<input required='required' type='integer' style='text-align:right;width:64px;' name='s'/><span class='strokedAndShadowed' style='color:#adadad;font-size:18px;margin-right:16px;margin-left:8px;'>s</span>");
	parent.append("<input required='required' type='integer' style='text-align:right;width:64px;' name='c'/><span class='strokedAndShadowed' style='color:#d66e26;font-size:18px;margin-right:16px;margin-left:8px;'>c</span>");

	parent.find("[name='g']").val(this.g);
	parent.find("[name='s']").val(this.s);
	parent.find("[name='c']").val(this.c);
};

net.lugdunon.currency.gsc.Currency.prototype.valueFromEditUI=function(parent)
{
	this.g=parseInt(parent.find("[name='g']").val());
	this.s=parseInt(parent.find("[name='s']").val());
	this.c=parseInt(parent.find("[name='c']").val());
};