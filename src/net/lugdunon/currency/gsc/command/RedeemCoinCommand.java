package net.lugdunon.currency.gsc.command;

import java.io.IOException;

import net.lugdunon.command.Command;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.IServerInvokedCommand;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.command.ex.ConsoleFiredCommandNotSupportedException;
import net.lugdunon.command.ex.IsNotServerInvokedCommand;
import net.lugdunon.currency.gsc.Currency;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.state.item.ItemDefinitions.ItemDefAndStackSize;
import net.lugdunon.state.item.ItemInstance;

import org.json.JSONException;

public class RedeemCoinCommand extends Command implements IServerInvokedCommand
{
	protected Response iRes;

	////
	
	@Override
	public String getCommandId()
	{
	    return("CORE.COMMAND.REDEEM.COIN");
	}

	@Override
	public String getName()
	{
		return("Redeems a Coin for its face value.");
	}

	@Override
	public String getDescription()
	{
		return("Redeems a Coin for its face value.");
	}

	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

	@Override
	public void handle(CommandRequest request) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		try
		{
			CommandProperties        props =new CommandProperties();
			int                      index =request.getData().read();
			Character                c     =request.getOrigin().getAccount().getActiveCharacter();

			props.setCharacter("character",c    );
			props.setInt      ("slotIndex",index);
			
			handle(props);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
    public void handle(CommandProperties props) throws IOException, ConsoleFiredCommandNotSupportedException
    {
//		String       d        ="";
		Character    c        =props.getCharacter("character"                          );
		int          slotIndex=props.getInt      ("slotIndex"                          );
		ItemInstance coin     =c.getInventoryItem(slotIndex,Character.ACTION_PLAY_BLOCK);
		
		try
        {
			CommandProperties riiProps   =new CommandProperties();
			
			if(c instanceof PlayerCharacter)
			{
				riiProps.setAccount("account",((PlayerCharacter) c).getAccount());
			}
			
			riiProps.setCharacter("character",c);
			riiProps.setInt      ("count",    coin.getItemDef().getToolDef().getInt("consumes"));
			riiProps.setInt      ("slotIndex",slotIndex                  );
			riiProps.setInt      ("slotType", Character.ACTION_PLAY_BLOCK);
			
            State.instance().getGame().addIncomingRequest(
        		"CORE.COMMAND.REMOVE.INVENTORY.ITEM",
        		riiProps
            );
        }
        catch (CommandNotSupportedException e)
        {
            e.printStackTrace();
        }
        catch (IsNotServerInvokedCommand e)
        {
            e.printStackTrace();
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }
		
		try
        {
			CommandProperties acProps =new CommandProperties();
			Currency          currency=new Currency         ();
			
			if("COPPER".equals(coin.getItemDef().getItemSubType()))
			{
//				d="copper";
				currency.setCopper(1);
			}
			else if("SILVER".equals(coin.getItemDef().getItemSubType()))
			{
//				d="silver";
				currency.setSilver(1);
			}
			else if("GOLD".equals(coin.getItemDef().getItemSubType()))
			{
//				d="gold";
				currency.setGold  (1);
			}
			
			acProps.setCharacter("character",c                                         );
			acProps.setCurrency ("currency", currency                                  );
			acProps.set         ("reason",   new ItemDefAndStackSize(coin.getItemDef()));
			
            State.instance().getGame().addIncomingRequest(
        		"CORE.COMMAND.ALTER.CURRENCY",
        		acProps
            );
        }
        catch (CommandNotSupportedException e)
        {
            e.printStackTrace();
        }
        catch (IsNotServerInvokedCommand e)
        {
            e.printStackTrace();
        }
		
//		try
//        {
//			CommandProperties lcProps=new CommandProperties();
//			
//			lcProps.setPlayerCharacter("player", (PlayerCharacter) c);
//			lcProps.setString         ("message","You received 1 "+d+".");
//			lcProps.setString         ("color",  "#0F0");
//			
//            State.instance().getGame().addIncomingRequest(
//        		"CORE.COMMAND.LOG.CONSOLE",
//        		lcProps
//            );
//        }
//        catch (CommandNotSupportedException e)
//        {
//            e.printStackTrace();
//        }
//        catch (IsNotServerInvokedCommand e)
//        {
//            e.printStackTrace();
//        }
    }
}