Namespace.declare("net.lugdunon.currency.gsc.command");

Namespace.newClass("net.lugdunon.currency.gsc.command.RedeemCoinCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.currency.gsc.command.RedeemCoinCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.currency.gsc.command.RedeemCoinCommand,"init",[initData]);

	this.actionBarIndex;
	
	return(this);
};

net.lugdunon.currency.gsc.command.RedeemCoinCommand.prototype.opInit=function(initData)
{
	this.actionBarIndex=initData.actionBarIndex;
};

net.lugdunon.currency.gsc.command.RedeemCoinCommand.prototype.getCommandLength=function()
{
    return(1);
};

net.lugdunon.currency.gsc.command.RedeemCoinCommand.prototype.buildCommand=function(dataView)
{

	dataView.writeUint8(this.actionBarIndex);
};

net.lugdunon.currency.gsc.command.RedeemCoinCommand.prototype.commandResponse=function(res)
{
	;
};