package net.lugdunon.currency.gsc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.lugdunon.io.ObjectToByteArray;
import net.lugdunon.io.ToBinary;
import net.lugdunon.state.State;
import net.lugdunon.state.currency.ICurrency;
import net.lugdunon.state.item.Item;
import net.lugdunon.state.item.ItemDefinitions.ItemDefAndStackSize;
import net.lugdunon.state.item.ItemInstance;

public class Currency implements ICurrency, ToBinary
{
	private static final Pattern PATTERN_GOLD  =Pattern.compile("([0-9]+)([gG]{1})");
	private static final Pattern PATTERN_SILVER=Pattern.compile("([0-9]+)([sS]{1})");
	private static final Pattern PATTERN_COPPER=Pattern.compile("([0-9]+)([cC]{1})");
	
	private int gold;
	private int silver;
	private int copper;
	
	private Currency(int gold, int silver, int copper)
	{
		setGold  (gold  );
		setSilver(silver);
		setCopper(copper);
	}
	
	public Currency()
	{
		this(0,0,0);
	}
	
	////
	
	@Override
	public ICurrency clone()
	{
		return(new Currency(gold,silver,copper));
	}
	
	////
	
	private void redistribute()
	{
		int r;
		int m;
		
		if(copper > 100)
		{
			m=copper%100;
			r=(copper-m)/100;
			
			copper =m;
			silver+=r;
		}
		
		if(silver > 100)
		{
			m=silver%100;
			r=(silver-m)/100;
			
			silver =m;
			gold  +=r;
		}
	}
	
	////

	@Override
	public void add(ICurrency c)
	{
		this.add(
			((Currency) c).getGold  (),
			((Currency) c).getSilver(),
			((Currency) c).getCopper()
		);
	}
	
	public void add(int gold, int silver, int copper)
	{
		addCopper(copper);
		addSilver(silver);
		addGold  (gold  );
	}
	
	public int addGold(int gold)
	{
		setGold(this.gold+gold);
		
		return(getGold());
	}
	
	public int addSilver(int silver)
	{
		setSilver(this.silver+silver);
		
		return(getSilver());
	}
	
	public int addCopper(int copper)
	{
		setCopper(this.copper+copper);
		
		return(getCopper());
	}
	
	////

	@Override
	public void subtract(ICurrency c)
	{
		this.add(
			-((Currency) c).getGold  (),
			-((Currency) c).getSilver(),
			-((Currency) c).getCopper()
		);
	}
	
	public void subtract(int gold, int silver, int copper)
	{
		subtractCopper(copper);
		subtractSilver(silver);
		subtractGold  (gold  );
	}
	
	public int subtractGold(int gold)
	{
		setGold(this.gold-gold);
		
		return(getGold());
	}
	
	public int subtractSilver(int silver)
	{
		setSilver(this.silver-silver);
		
		return(getSilver());
	}
	
	public int subtractCopper(int copper)
	{
		setCopper(this.copper-copper);
		
		return(getCopper());
	}
	
	////

	@Override
	public void multiply(int multiplier)
	{
		fromPrimitive(toPrimitive()*multiplier);
	}

	@Override
	public void multiply(double multiplier)
	{
		fromPrimitive(Math.round(toPrimitive()*multiplier));
	}
	
	////
	
	public int getGold()
    {
    	return gold;
    }

	public void setGold(int gold)
    {
    	this.gold = gold;
    }

	public int getSilver()
    {
    	return silver;
    }

	public void setSilver(int silver)
    {
    	this.silver = silver;
    	redistribute();
    }

	public int getCopper()
    {
    	return copper;
    }

	public void setCopper(int copper)
    {
    	this.copper = copper;
    	redistribute();
    }
	
	////

	@Override
	public long toPrimitive()
	{
		return((getGold()*10000)+(getSilver()*100)+getCopper());
	}

	@Override
	public void fromPrimitive(long value)
	{
		setGold  ((int) (value/10000));
		setSilver((int) ((value-(getGold()*10000))/100));
		setCopper((int) (value-((getGold()*10000)+(getSilver()*100))));
	}

	@Override
	public boolean fromString(String value)
	{
		Matcher m=PATTERN_GOLD.matcher(value);

		if(m.find())
		{
			setGold  (Integer.parseInt(m.group(1),10));
		}
		
		m=PATTERN_SILVER.matcher(value);
		
		if(m.find())
		{
			setSilver(Integer.parseInt(m.group(1),10));
		}
		
		m=PATTERN_COPPER.matcher(value);
		
		if(m.find())
		{
			setCopper(Integer.parseInt(m.group(1),10));
		}
		
		return(value.indexOf('-')!=0);
	}

	@Override
	public String toString()
	{
		return(getGold()+"g "+getSilver()+"s "+getCopper()+"c");
	}
	
	////

	@Override
    public List<ItemDefAndStackSize> toItems()
    {
		List<ItemDefAndStackSize> value=new ArrayList<ItemDefAndStackSize>();
		
		convertToCoin(
			value,
			State.instance().getWorld().getItemDefinitions().getItemDef("COIN.COPPER"),
			getCopper()
		);
		
		convertToCoin(
			value,
			State.instance().getWorld().getItemDefinitions().getItemDef("COIN.SILVER"),
			getSilver()
		);
		
		convertToCoin(
			value,
			State.instance().getWorld().getItemDefinitions().getItemDef("COIN.GOLD"  ),
			getGold  ()
		);
		
	    return(value);
    }
	
	private void convertToCoin(List<ItemDefAndStackSize> coins, Item coin, int value)
	{
		int fullStacks=value / Item.MAX_STACK_SIZE;
		int remainder =value % Item.MAX_STACK_SIZE;
		
		for(int i=0;i<fullStacks;i++)
		{
			coins.add(new ItemDefAndStackSize(coin, Item.MAX_STACK_SIZE));
		}
		
		if(remainder > 0)
		{
			coins.add(new ItemDefAndStackSize(coin, remainder          ));
		}
	}

	@Override
    public void fromItems(List<ItemInstance> value)
    {
	    for(ItemInstance v:value)
	    {
	    	if("COIN.COPPER".equals(v.getItemDef().getItemId()))
	    	{
	    		addCopper(1);
	    	}
	    	else if("COIN.SILVER".equals(v.getItemDef().getItemId()))
	    	{
	    		addSilver(1);
	    	}
	    	else if("COIN.GOLD".equals(v.getItemDef().getItemId()))
	    	{
	    		addGold  (1);
	    	}
	    }
    }
	
	////

	@Override
    public byte[] toBytes()
    {
	    ObjectToByteArray otb=new ObjectToByteArray();
	    
	    try
	    {
		    otb.writeDouble(toPrimitive());
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    finally
	    {
	    	try
            {
	            otb.close();
            }
            catch (IOException e)
            {
	            e.printStackTrace();
            }
	    }
	    
	    return(otb.toBytes());
    }
}
