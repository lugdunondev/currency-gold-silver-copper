Namespace.declare("net.lugdunon.currency.gsc.action");
Namespace.newClass("net.lugdunon.currency.gsc.action.RedeemCoinActionHandler","net.lugdunon.state.item.action.IActionHandler");

////

net.lugdunon.currency.gsc.action.RedeemCoinActionHandler.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.currency.gsc.action.RedeemCoinActionHandler,"init",[initData]);
	
	return(this);
};

net.lugdunon.currency.gsc.action.RedeemCoinActionHandler.prototype.renderToolUseInformation=function(toolDef)
{
	return("<div class='ttSubLineItem'>On use, redeems this coin for currency.</div>");
};

net.lugdunon.currency.gsc.action.RedeemCoinActionHandler.prototype.getTriggerMode=function()
{
	return(net.lugdunon.state.item.action.IActionHandler.TRIGGER_MODE_SIMPLE);
};

net.lugdunon.currency.gsc.action.RedeemCoinActionHandler.prototype.handleUse=function(character,placeable,cursorLoc,actionBarIndex,inputBegan,inputMethod)
{
//	if(!this.callSuper(net.lugdunon.currency.gsc.action.RedeemCoinActionHandler,"handleUse",[character,placeable,cursorLoc,actionBarIndex,inputBegan,inputMethod]))
//	{
//		return(false);
//	}
	
	//REDEEM COIN
	game.client.sendCommand(
		game.client.buildCommand(
			"CORE.COMMAND.REDEEM.COIN",
			{
				actionBarIndex:actionBarIndex
			}
		)
	);
	
	return(true);
};